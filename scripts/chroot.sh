#!/bin/bash

# Copyright (C) 2019 Robin Wils

# These scripts are free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# These scripts are distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with these scripts.  If not, see <https://www.gnu.org/licenses/>.


# SCRIPT
# ______


if [ "$EUID" -ne 0 ]
then echo "You need to be root to execute this script."
     exit 1
fi

source tmp/scripts/config.sh

echo "Creating a fresh /etc/apt/sources.list..."

echo "deb $PACKAGE_REPOSITORY $RELEASE main" \
     > /etc/apt/sources.list
echo "deb $PACKAGE_REPOSITORY $RELEASE-updates main" \
     >> /etc/apt/sources.list
echo "deb $PACKAGE_REPOSITORY $RELEASE-security main" \
     >> /etc/apt/sources.list
echo "deb $PACKAGE_REPOSITORY $RELEASE-backports main" \
     >> /etc/apt/sources.list
touch /etc/apt/apt.conf.d/01lean
echo "APT::Install-Recommends \"0\";" \
     >> /etc/apt/apt.conf.d/01lean
echo "APT::AutoRemove::RecommendsImportant \"false\";" \
     >> /etc/apt/apt.conf.d/01lean

echo "Updating repositories..."
apt-get update > /dev/null

echo "Install keyring..."
apt-get --allow-unauthenticated \
        -yqq install $KEYRING_PACKAGE > /dev/null

echo "Updating repositories..."
apt-get clean && apt-get update > /dev/null

echo "Do a dist-upgrade..."
apt-get -yqq dist-upgrade > /dev/null

echo "Installing packages (this can take a while)..."
apt-get -yqq install $ESSENTIAL_APT_PACKAGES > /dev/null
apt-get -yqq install $APT_PACKAGES_WITH_DIALOGS > /dev/null
apt-get -yqq install $NETWORKING_APT_PACKAGES > /dev/null
apt-get -yqq install $RECOMMENDED_APT_PACKAGES > /dev/null
apt-get -yqq install $AUDIO_APT_PACKAGES $APT_PACKAGES > /dev/null
apt-get -yqq install $XORG_DRIVERS > /dev/null
apt-get -yqq install $XORG_INPUT_DRIVERS > /dev/null

echo "Copying config files..."
cp -r tmp/scripts/config-files/. ~/.config

OPENBOX_MENU="~/.config/openbox/menu.xml"
if [[ $PREFERRED_EDITOR != "emacs" ]];
then
    sed -i "s/GNU Emacs/$PREFERRED_EDITOR/" $OPENBOX_MENU
    sed -i "s/emacsclient -a '' -c/$PREFERRED_EDITOR/" $OPENBOX_MENU
    if [[ $PREFERRED_EDITOR != *vim* ]];
    then
        # Many editors can't open directories -
        # Remove open config in preferred editor and 
        # preferred editor from openbox menu.
        sed -i '25,29d' $OPENBOX_MENU
        sed -i '19,23d' $OPENBOX_MENU
    fi
    if [[ $PREFERRED_EDITOR == *vi* ]];
    then
        echo ">:) Your preferred editor contains the text \"vi\". >:)"
        echo "EVIL EDITOR DETECTED, but we will install it"
        echo "on your system just like a nice person would do."
    fi
    apt-get -yqq install $PREFERRED_EDITOR > /dev/null
else
    echo "Installing emacs..."
    echo "deb-src $PACKAGE_REPOSITORY $RELEASE main" >> /etc/apt/sources.list
    apt-get update > /dev/null
    apt-get -yqq install build-essential > /dev/null
    apt-get -yqq build-dep emacs25 > /dev/null
    mkdir emacs && cd emacs && wget $EMACS_TAR --no-check-certificate > /dev/null
    tar -zxvf *.tar.gz && cd */ > /dev/null
    ./autogen.sh all > /dev/null
    ./configure --with-mailutils > /dev/null
    make && make install > /dev/null
    cd ../.. && rm -r emacs

    echo "Configuring emacs..."
    wget --no-check-certificate \
        -O ~/.emacs \
        https://gitlab.com/RobinWils/dotfiles/raw/master/emacs/.emacs \
        -q > /dev/null
    mkdir ~/.emacs.d
    wget --no-check-certificate \
        -O ~/.emacs.d/emacs-init.org \
        https://gitlab.com/RobinWils/dotfiles/raw/master/emacs/emacs-init.org \
        -q > /dev/null
fi

if [[ $PREFERRED_WEB_BROWSER != "icecat" ]];
then
    apt-get -yqq install $PREFERRED_WEB_BROWSER > /dev/null
    sed -i "s/icecat/$PREFERRED_WEB_BROWSER/" $OPENBOX_MENU > /dev/null
else
    echo "Installing GNU icecat..."
    wget -O /tmp/icecat.tar.bz2 $GNU_ICECAT_TAR --no-check-certificate > /dev/null
    # TODO add signature checking
    # wget -O /tmp/icecat.tar.bz2.sig $GNU_ICECAT_TAR.sig > /dev/null
    tar -xjf /tmp/icecat.tar.bz2 -C /opt/ > /dev/null
    ln -sf /opt/icecat/icecat /usr/local/bin/icecat
    rm -rf /tmp/icecat.tar.bz2
    /bin/cat <<EOM >/usr/share/applications/icecat.desktop
[Desktop Entry]
Version=1.0
Type=Application
Name=GNU Icecat
Comment=Browse the World Wide Web
Exec=/opt/icecat/icecat
Terminal=false
X-MultipleArgs=false
Icon=/opt/icecat/browser/chrome/icons/default/default16.png
Categories=Network;WebBrowser;
EOM
fi

if [[ $AUDIO_APT_PACKAGES == *"pulseaudio"* ]];
then
    if [[ `lscpu | grep 'Byte Order'` == *"Little Endian"* ]];
    then
        # Use Little Endian audio settings
        echo "default-sample-format = float32le" > /etc/pulse/daemon.conf
    else
        # Use Big Endian audio settings
        echo "default-sample-format = float32be" > /etc/pulse/daemon.conf
    fi
    echo "default-sample-rate = 48000" >> /etc/pulse/daemon.conf
    echo "alternate-sample-rate = 44100" >> /etc/pulse/daemon.conf
    echo "default-sample-channels = 2" >> /etc/pulse/daemon.conf
    echo "default-channel-map = front-left,front-right" >> /etc/pulse/daemon.conf
    echo "default-fragments = 2" >> /etc/pulse/daemon.conf
    echo "default-fragment-size-msec = 125" >> /etc/pulse/daemon.conf
    echo "resample-method = soxr-vhq" >> /etc/pulse/daemon.conf
    echo "enable-lfe-remixing = no" >> /etc/pulse/daemon.conf
    echo "high-priority = yes" >> /etc/pulse/daemon.conf
    echo "nice-level = -11" >> /etc/pulse/daemon.conf
    echo "realtime-scheduling = yes" >> /etc/pulse/daemon.conf
    echo "realtime-priority = 9" >> /etc/pulse/daemon.conf
    echo "rlimit-rtprio = 9" >> /etc/pulse/daemon.conf
    echo "daemonize = no" >> /etc/pulse/daemon.conf

    /bin/cat <<EOM >/etc/asound.conf
# Use PulseAudio plugin hw
pcm.!default {
   type plug
   slave.pcm hw
}
EOM
fi
echo "keycode 67 = XF86AudioMute" > ~/.xmodmaprc
echo "keycode 68 = XF86AudioLowerVolume" >> ~/.xmodmaprc
echo "keycode 69 = XF86AudioRaiseVolume" >> ~/.xmodmaprc

apt-get -yqq install $PREFERRED_TERMINAL > /dev/null
sed -i "s/x-terminal-emulator/$PREFERRED_TERMINAL/" \
    $OPENBOX_MENU > /dev/null

echo "Autoremove unneeded packages..."
apt-get -yqq autoremove --purge > /dev/null

echo "Making xinitrc file..."
touch ~/.xinitrc
echo "exec openbox-session" > ~/.xinitrc

echo "Setting root password..."
echo -e "root\nroot" | passwd root > /dev/null
passwd -e root > /dev/null

echo "Creating user..."
useradd -m user -s /bin/bash
USERGROUPS="cdrom console floppy sudo audio video plugdev netdev tty input user"
for group in $USERGROUPS
do
    groupadd $group
    usermod -aG $group user
done

echo -e "user\nuser" | passwd user > /dev/null
passwd -e user > /dev/null

echo "Copying wallpapers..."
mkdir ~/Pictures
mkdir ~/Pictures/Wallpapers
cp -r /tmp/scripts/default-wallpaper.png \
   ~/Pictures/Wallpapers/default-wallpaper.png > /dev/null

echo "Setting wicd settings..."
cat /tmp/scripts/wicd/manager-settings.conf \
    > /etc/wicd/manager-settings.conf

echo "Configuring git..."
git config --global core.editor $PREFERRED_EDITOR
git config --global user.email $GIT_EMAIL
git config --global user.name $GIT_EMAIL

echo "Configuring green on black terminal colors..."
touch ~/.Xdefaults
echo "xterm*background: black" >> ~/.Xdefaults
echo "xterm*foreground: green" >> ~/.Xdefaults

if [[ $HIDE_GRUB = true ]];
then
    echo "Updating grub..."
    /bin/cat <<EOM >/etc/default/grub
GRUB_DEFAULT=0
GRUB_HIDDEN_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT_QUIET=true
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR="$GRUB_DISTRIBUTOR"
GRUB_CMDLINE_LINUX_DEFAULT="quiet"
GRUB_CMDLINE_LINUX=""
EOM
    update-grub > /dev/null
fi

[ -z "$XKBOPTIONS" ] ||
    (echo "Configuring keybindings..." \
         && sed -i "/XKBOPTIONS/c\XKBOPTIONS=\"$XKBOPTIONS\"" \
                /etc/default/keyboard)

echo "Setting the hostname..."
echo $HOSTNAME > /etc/hostname

echo "Replacing the default host file..."
wget -O /etc/hosts $HOSTS -q > /dev/null
# Add hostname to hosts file, since some programs depend on this line.
sed -i "/^127.0.0.1\\ localhost.localdomain/i 127.0.0.1\\ $HOSTNAME" \
    /etc/hosts

echo "Copying files from root to user..."
cp -r ~/. ~user/
echo "Making sure that user owns everything in /home/user..."
chown -R user /home/user/*
chown -R user /home/user/.*

echo "Running the after-install script..."
source tmp/scripts/after-install.sh
