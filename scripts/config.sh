#!/bin/bash

# Copyright (C) 2019 Robin Wils

# This config is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This config is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this config if not see <https://www.gnu.org/licenses/>.

# CONFIG
# ______

readonly PARTITION_LABEL="root"
readonly RELEASE="ascii"
readonly DEBOOTSTRAP_RELEASE="stable"
readonly PACKAGE_REPOSITORY="http://deb.devuan.org/merged"
readonly KEYRING_PACKAGE="devuan-keyring"
# Don't use readonly, since the variable probably already has a value
HOSTNAME="thonkpad"

# Custom host file (which blocks ads) 
readonly HOSTS="https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
readonly GIT_EMAIL="mrwils@tutanota.com"

# Keybindings
# Replacing caps with ctrl - only recommended for 
# bad keyboards like laptop keyboards. Keep this variable 
# empty if you don't want any new keybindings
readonly XKBOPTIONS="ctrl:nocaps"
# GRUB
readonly HIDE_GRUB=true
# Name in grub menu
readonly GRUB_DISTRIBUTOR="My Epic Distro"

# PREFERRED VARIABLES
# Preferred variables may only contain one package. 
# The script will use apt to install the package, 
# if the package isn't emacs or icecat.
readonly PREFERRED_EDITOR="emacs"
# Only needed for (the latest) emacs
readonly EMACS_TAR="https://ftp.gnu.org/pub/gnu/emacs/emacs-26.2.tar.gz"

readonly PREFERRED_WEB_BROWSER="icecat"
# Only needed for icecat
readonly GNU_ICECAT_TAR="https://ftp.gnu.org/gnu/gnuzilla/60.7.0/icecat-60.7.0.en-US.gnulinux-x86_64.tar.bz2"

readonly PREFERRED_TERMINAL="xterm"

# APT
# Apt will install these packages
readonly APT_PACKAGES="baobab gimp keepassxc mplayer pinta"

# These packages are the first ones which will install so that people
# can do something else while the script is running. These
# packages are essential. Console-setup is probably needed if
# you don't use QWERTY like me.
readonly APT_PACKAGES_WITH_DIALOGS="console-setup grub2"

readonly ESSENTIAL_APT_PACKAGES="git fonts-hack-ttf linux-image-`dpkg --print-architecture` openbox tar whiptail wget"

readonly RECOMMENDED_APT_PACKAGES="arandr acpi bzip2 hsetroot htop maim pm-utils rsync sct slop stalonetray suckless-tools zip"

# You can use alsa-base if you want a more minimal setup,
# ALSA might be harder to configure though.
readonly AUDIO_APT_PACKAGES="pavucontrol pulseaudio"

# connection setup info
# more minimal as wicd: isc-dhcp-client net-tools wpasupplicant
# https://guix.gnu.org/manual/en/html_node/Keyboard-Layout-and-Networking-and-Partitioning.html#Keyboard-Layout-and-Networking-and-Partitioning
readonly NETWORKING_APT_PACKAGES="wicd wicd-gtk"

# XORG DRIVERS GUIDE

# Recommended to keep:
# xserver-xorg-video-dummy xserver-xorg-input-void xserver-xorg-core
# xinit x11-xserver-utils

# xserver-xorg-legacy is usually needed to run X as a regular user
#for some reason

# Pick one supported driver or more supported drivers of these:
# Already included - xserver-xorg-video-intel (intel)
# Already included - xserver-xorg-video-nouveau (nvidia)
#                    xserver-xorg-video-openchrome (via)
#                    xserver-xorg-video-radeon (amd)
#                    xserver-xorg-video-vesa (generic display driver)

readonly XORG_DRIVERS="xserver-xorg-video-dummy xserver-xorg-input-void xserver-xorg-core xserver-xorg-legacy xinit x11-xserver-utils xserver-xorg-video-intel xserver-xorg-video-nouveau"

# INPUT DRIVERS

# The default included evdev driver works for many input devices.

# Mouse and keyboard support separately (if evdev is not good enough):
# xserver-xorg-input-mouse xserver-xorg-input-kbd

# Touchpad drivers:
# xserver-xorg-input-synaptics

readonly XORG_INPUT_DRIVERS="xserver-xorg-input-evdev xserver-xorg-input-synaptics"

# You can add any extras to those lists if you wish to do so
# xfonts is one of those extras
