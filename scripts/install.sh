#!/bin/bash

# Copyright (C) 2019 Robin Wils

# These scripts are free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# These scripts are distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with these scripts.  If not, see <https://www.gnu.org/licenses/>.

# SCRIPT
# ------
# LOAD THE CONFIG
source config.sh

# CHECK IF USER HAS ROOT PERMISSIONS
if [ "$EUID" -ne 0 ]
then echo "You need to be root to execute this script."
     exit 1
fi

# MOUNTPOINT SETUP
# make temporary mountpoint
mkdir -p /tmp/debootstrap

# check if our partition exists
partitionExists=`lsblk -o label | grep $PARTITION_LABEL`
if [ -z "$partitionExists" ]; then
    echo "Couldn't find a partition with the label $PARTITION_LABEL"
    exit 1
fi

# mount our partition
mount -L $PARTITION_LABEL /tmp/debootstrap &> /dev/null

# DEBOOTSTRAP SETUP
echo "Running Debootstrap (this can take a while)..."
debootstrap --variant=minbase \
            $DEBOOTSTRAP_RELEASE /tmp/debootstrap \
            $PACKAGE_REPOSITORY > /dev/null

mount -t proc /proc /tmp/debootstrap/proc
mount -o bind /dev /tmp/debootstrap/dev
mount -o bind /sys /tmp/debootstrap/sys
mkdir -p /tmp/debootstrap/tmp/scripts
cp -r ../scripts/. /tmp/debootstrap/tmp/scripts

# chroot into the target
chroot /tmp/debootstrap /bin/bash ./tmp/scripts/chroot.sh 

# remove copy of scripts folder
rm -r /tmp/debootstrap/tmp/scripts

# unmount proc, dev and sys
umount /tmp/debootstrap/proc
umount /tmp/debootstrap/dev
umount /tmp/debootstrap/sys
umount /tmp/debootstrap

echo -e "\n\nInstallation complete, please reboot."
